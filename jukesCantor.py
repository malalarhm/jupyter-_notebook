import random
import math as M

def jukesCantor(genome,gen_time):
  """
  Input: genome, a character string consisting of a, c, g, and t
         gen_time, genetic time
  Output: a randomly modified string according to the Juke's cantor model
    The following coupled ODEs are integrated from t=0 to t=gen_time

    dot a = -a + (c + g + t) /3
    dot c = -a + (g + t + a) /3
    dot g = -g + (t + a + c) /3
    dot t = -t + (a + c + g) /3
  
    using the solution 
 
    a(t)=(3/4)*exp(-t)*(a(0)-(c(0)+g(0)+t(0))/3)
        +(1/4)*(a(0)+c(0)+g(0)+t(0))

    and similarly for c(t), g(t), and t(t).

    In other words, if 'a' is the initial base, its persistence
    probability after a time t is 

    3/4 exp(-t) + 1/4

    and likewise for the other three base inputs.

  """
  if type(gen_time) !=  float :
    raise Exception("Time must be a float")
  for b in genome:
    if not ( b=='a' or b=='c' or b=='g' or b=='t' ):
       raise Exception("Invalid input genome")
  persistence_prob=(3.*M.exp(-gen_time)+1.)/4.
  new_genome=[]
  print("Genome=",genome)
  for base in genome:
    p=random.random()
    if p < persistence_prob:
      new_base=base
    else: 
      if base=='a':
        seq=('c','g','t')
      if base=='c':
        seq=('a','g','t')
      if base=='g':
        seq=('a','c','t')
      if base=='t':
        seq=('a','c','g')
      new_base=random.choice(seq)
    new_genome.append(new_base)
  return(new_genome)

